package id.co.nexsoft.restapi.service;

import java.util.List;

public interface RelationStringService<T> {
    List<T> getDataByRelId(String userId);
}
