package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.ShopAddress;
import id.co.nexsoft.restapi.repository.ShopAddressRepository;
import id.co.nexsoft.restapi.service.ObjectLongService;
import id.co.nexsoft.restapi.service.RelationLongService;
import id.co.nexsoft.restapi.service.LongService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ShopAddressServiceImpl implements LongService<ShopAddress>, ObjectLongService<ShopAddress>, RelationLongService<ShopAddress> {
    @Autowired
    private ShopAddressRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<ShopAddress> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<ShopAddress> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public List<ShopAddress> getAllNonActiveData() {
        return repo.findAllByDeletedDateIsNotNull();
    }

    @Override
    public Optional<ShopAddress> getDataById(Long id) {
        return repo.findById(id);
    }

    @Override
    public Optional<ShopAddress> getActiveDataById(Long id) {
        return repo.findActiveDataById(id);
    }

    @Override
    public Optional<ShopAddress> getNonActiveDataById(Long id) {
        return repo.findNonActiveDataById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<ShopAddress> ShopAddress = repo.findActiveDataById(id);
        if (!ShopAddress.isPresent()) {
            return false;
        }
        ShopAddress.get().setDeletedDate(LocalDateTime.now());
        return true;
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE shop_address SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public ShopAddress putData(ShopAddress data, Long id) {
        Optional<ShopAddress> ShopAddress = repo.findById(id);
        if (!ShopAddress.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public ShopAddress postData(ShopAddress data) {
        data.setCreatedDate(LocalDateTime.now());
        return repo.save(data);
    }   
    
    @Override
    public List<ShopAddress> getDataByRelId(Long userId) {
        return repo.getDataByShopId(userId);
    }
}
