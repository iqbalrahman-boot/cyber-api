package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.User;
import id.co.nexsoft.restapi.repository.UserRepository;
import id.co.nexsoft.restapi.service.CredentialService;
import id.co.nexsoft.restapi.service.ObjectStringService;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.utils.GetPassword;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class UserServiceImpl extends GetPassword implements UUIDService<User>, ObjectStringService<User>, CredentialService<User> {
    @Autowired
    private UserRepository repo;

    // @Autowired
    // private PasswordEncoder passwordEncoder;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<User> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<User> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public List<User> getAllNonActiveData() {
        return repo.findAllByDeletedDateIsNotNull();
    }

    @Override
    public Optional<User> getDataById(String id) {
        return repo.findById(id);
    }

    @Override
    public Optional<User> getActiveDataById(String id) {
        return repo.findActiveDataById(id);
    }

    @Override
    public Optional<User> getNonActiveDataById(String id) {
        return repo.findNonActiveDataById(id);
    }

    @Override
    public boolean deleteDataById(String id) {
        Optional<User> user = repo.findActiveDataById(id);
        if (!user.isPresent()) {
            return false;
        }
        user.get().setDeletedDate(LocalDateTime.now());
        return true;
    }

    @Override
    public User putData(User data, String id) {
        Optional<User> users = repo.findById(id);
        if (!users.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public User postData(User data) {
        // data.setPassword(passwordEncoder.encode(data.getPassword()));
        data.setCreatedDate(LocalDateTime.now());
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, String id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE user SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<User> getAllDataByEmail(String email) {
        return repo.findAllByEmail(email);
    }

    @Override
    public List<User> getAllDataByPhone(String phone) {
        return repo.findAllByPhone(phone);
    }
}
