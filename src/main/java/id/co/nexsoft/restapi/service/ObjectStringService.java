package id.co.nexsoft.restapi.service;

public interface ObjectStringService<T> {
    T postData(T data);
    T putData(T data, String id);
}
