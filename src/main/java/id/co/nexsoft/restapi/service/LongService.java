package id.co.nexsoft.restapi.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LongService<T> {
    List<T> getAllData();
    List<T> getAllActiveData();
    List<T> getAllNonActiveData();
    Optional<T> getDataById(Long id);
    Optional<T> getActiveDataById(Long id);
    Optional<T> getNonActiveDataById(Long id);
    boolean deleteDataById(Long id);
    void patchData(Map<String, Object> data, Long id);
}
