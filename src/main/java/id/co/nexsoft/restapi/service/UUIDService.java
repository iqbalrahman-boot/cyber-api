package id.co.nexsoft.restapi.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UUIDService<T> {
    List<T> getAllData();
    List<T> getAllActiveData();
    List<T> getAllNonActiveData();
    Optional<T> getDataById(String id);
    Optional<T> getActiveDataById(String id);
    Optional<T> getNonActiveDataById(String id);
    boolean deleteDataById(String id);
    void patchData(Map<String, Object> data, String id);
}
