package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Cart;
import id.co.nexsoft.restapi.repository.CartRepository;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.SimpleStringService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class CartServiceImpl implements SimpleStringService<Cart>, RelationStringService<Cart> {
    @Autowired
    private CartRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Cart> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<Cart> getDataById(String id) {
        return repo.findById(id);
    }


    @Override
    public boolean deleteDataById(String id) {
        Optional<Cart> Cart = repo.findById(id);
        if (!Cart.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public Cart putData(Cart data, String id) {
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Cart postData(Cart data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, String id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Cart SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Cart> getDataByRelId(String userId) {
        return repo.findAllByRelId(userId);
    }
}