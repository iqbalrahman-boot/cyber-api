package id.co.nexsoft.restapi.service;

import java.util.List;

public interface RelationLongService<T> {
    List<T> getDataByRelId(Long userId);
}
