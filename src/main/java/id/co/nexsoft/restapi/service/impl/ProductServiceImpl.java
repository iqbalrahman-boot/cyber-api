package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Product;
import id.co.nexsoft.restapi.repository.ProductRepository;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.service.ObjectStringService;
import id.co.nexsoft.restapi.service.RelationLongService;
import id.co.nexsoft.restapi.utils.GetPassword;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class ProductServiceImpl extends GetPassword implements UUIDService<Product>, ObjectStringService<Product>, RelationLongService<Product> {
    @Autowired
    private ProductRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Product> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Product> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public List<Product> getAllNonActiveData() {
        return repo.findAllByDeletedDateIsNotNull();
    }

    @Override
    public Optional<Product> getDataById(String id) {
        return repo.findById(id);
    }

    @Override
    public Optional<Product> getActiveDataById(String id) {
        return repo.findActiveDataById(id);
    }

    @Override
    public Optional<Product> getNonActiveDataById(String id) {
        return repo.findNonActiveDataById(id);
    }

    @Override
    public boolean deleteDataById(String id) {
        Optional<Product> Product = repo.findActiveDataById(id);
        if (!Product.isPresent()) {
            return false;
        }
        Product.get().setDeletedDate(LocalDateTime.now());
        return true;
    }

    @Override
    public Product putData(Product data, String id) {
        Optional<Product> products = repo.findById(id);
        if (!products.isPresent()) {
            return null;
        }
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public Product postData(Product data) {
        data.setCreatedDate(LocalDateTime.now());
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, String id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE product SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public List<Product> getDataByRelId(Long userId) {
        return repo.findAllByUserId(userId);
    }
}