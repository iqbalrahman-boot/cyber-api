package id.co.nexsoft.restapi.service;

import java.util.Map;

public interface MapStringService<T> {
    T postData(Map<String, Object> data);
    T putData(Map<String, Object> data, String id);
}
