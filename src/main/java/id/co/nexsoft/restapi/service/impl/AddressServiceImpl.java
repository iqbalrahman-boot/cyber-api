package id.co.nexsoft.restapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.Address;
import id.co.nexsoft.restapi.repository.AddressRepository;
import id.co.nexsoft.restapi.service.MapStringService;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.UUIDService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class AddressServiceImpl implements UUIDService<Address>, MapStringService<Address>, RelationStringService<Address> {
    @Autowired
    private AddressRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Address> getAllData() {
        return repo.findAll();
    }

    @Override
    public List<Address> getAllActiveData() {
        return repo.findAllByDeletedDateIsNull();
    }

    @Override
    public List<Address> getAllNonActiveData() {
        return repo.findAllByDeletedDateIsNotNull();
    }

    @Override
    public Optional<Address> getDataById(String id) {
        return repo.findById(id);
    }

    @Override
    public Optional<Address> getActiveDataById(String id) {
        return repo.findActiveDataById(id);
    }

    @Override
    public Optional<Address> getNonActiveDataById(String id) {
        return repo.findNonActiveDataById(id);
    }

    @Override
    public boolean deleteDataById(String id) {
        Optional<Address> Address = repo.findActiveDataById(id);
        if (!Address.isPresent()) {
            return false;
        }
        Address.get().setDeletedDate(LocalDateTime.now());
        return true;
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, String id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE address SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public Address postData(Map<String, Object> data) {
        Address address = new Address(
            (String) data.get("userId") ,
            (String) data.get("street"), 
            (String) data.get("district"), 
            (String) data.get("city"), 
            (String) data.get("province"), 
            (String) data.get("country"), 
            (String) data.get("code"), 
            LocalDateTime.now(), null
        );
        return repo.save(address);
    }

    @Override
    public Address putData(Map<String, Object> data, String id) {
        Optional<Address> Address = repo.findById(id);
        if (!Address.isPresent()) {
            return null;
        }
        repo.putData(data, id);
        return Address.get();
    }

    @Override
    public List<Address> getDataByRelId(String userId) {
        return repo.getDataByUserId(userId);
    }   
}
