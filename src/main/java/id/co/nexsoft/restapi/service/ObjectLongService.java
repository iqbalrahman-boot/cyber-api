package id.co.nexsoft.restapi.service;

public interface ObjectLongService<T> {
    T postData(T data);
    T putData(T data, Long id);
}
