package id.co.nexsoft.restapi.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.model.PaymentDetail;
import id.co.nexsoft.restapi.repository.PaymentDetailRepository;
import id.co.nexsoft.restapi.service.SimpleService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class PaymentDetailServiceImpl implements SimpleService<PaymentDetail> {
    @Autowired
    private PaymentDetailRepository repo;

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<PaymentDetail> getAllData() {
        return repo.findAll();
    }

    @Override
    public Optional<PaymentDetail> getDataById(Long id) {
        return repo.findById(id);
    }


    @Override
    public boolean deleteDataById(Long id) {
        Optional<PaymentDetail> PaymentDetail = repo.findById(id);
        if (!PaymentDetail.isPresent()) {
            return false;
        }
        repo.deleteById(id);
        return true;
    }

    @Override
    public PaymentDetail putData(PaymentDetail data, Long id) {
        data.setId(id);
        return repo.save(data);
    }

    @Override
    public PaymentDetail postData(PaymentDetail data) {
        return repo.save(data);
    }

    @Transactional
    @Override
    public void patchData(Map<String, Object> data, Long id) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE PaymentDetail SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}