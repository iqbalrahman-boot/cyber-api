package id.co.nexsoft.restapi.service;

import java.util.List;

public interface CredentialService<T> {
    List<T> getAllDataByEmail(String email);
    List<T> getAllDataByPhone(String phone);
}
