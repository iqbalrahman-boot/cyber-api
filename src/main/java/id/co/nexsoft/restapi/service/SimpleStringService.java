package id.co.nexsoft.restapi.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface SimpleStringService<T> {
    List<T> getAllData();
    Optional<T> getDataById(String id);
    boolean deleteDataById(String id);
    void patchData(Map<String, Object> data, String id);
    T putData(T data, String id);
    T postData(T data);
}
