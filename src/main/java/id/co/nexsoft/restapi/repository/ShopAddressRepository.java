package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.ShopAddress;
import jakarta.transaction.Transactional;

public interface ShopAddressRepository extends JpaRepository<ShopAddress, Long> {
    List<ShopAddress> findAllByDeletedDateIsNull();
    List<ShopAddress> findAllByDeletedDateIsNotNull();

    @Query("SELECT u FROM ShopAddress u WHERE u.id = :id AND deletedDate IS NULL")
    Optional<ShopAddress> findActiveDataById(@Param("id") Long id);

    @Query("SELECT u FROM ShopAddress u WHERE u.id = :id AND deletedDate IS NOT NULL")
    Optional<ShopAddress> findNonActiveDataById(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query(value = "SELECT * FROM shop_address WHERE shop_id = :id", nativeQuery = true)
    List<ShopAddress> getDataByShopId(@Param("id") Long id);
}
