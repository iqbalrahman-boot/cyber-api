package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Shop;

public interface ShopRepository extends JpaRepository<Shop, Long> {
    List<Shop> findAllByDeletedDateIsNull();
    List<Shop> findAllByDeletedDateIsNotNull();
    List<Shop> findAllByEmail(String email);
    List<Shop> findAllByPhone(String phone);

    @Query("SELECT u FROM Shop u WHERE u.id = :id AND deletedDate IS NULL")
    Optional<Shop> findActiveDataById(@Param("id") Long id);

    @Query("SELECT u FROM Shop u WHERE u.id = :id AND deletedDate IS NOT NULL")
    Optional<Shop> findNonActiveDataById(@Param("id") Long id);
}
