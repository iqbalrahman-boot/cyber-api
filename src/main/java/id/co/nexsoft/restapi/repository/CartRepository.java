package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Cart;

public interface CartRepository extends JpaRepository<Cart, String> {
    @Query("SELECT u FROM Cart u WHERE u.userId = :id")
    List<Cart> findAllByRelId(@Param("id") String id);
}
