package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Address;
import jakarta.transaction.Transactional;

public interface AddressRepository extends JpaRepository<Address, String> {
    List<Address> findAllByDeletedDateIsNull();
    List<Address> findAllByDeletedDateIsNotNull();

    @Query("SELECT u FROM Address u WHERE u.id = :id AND deletedDate IS NULL")
    Optional<Address> findActiveDataById(@Param("id") String id);

    @Query("SELECT u FROM Address u WHERE u.id = :id AND deletedDate IS NOT NULL")
    Optional<Address> findNonActiveDataById(@Param("id") String id);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO address (user_id, street, district, city, province, country, code) VALUES (:#{#data['userId']}, :#{#data['street']}, :#{#data['district']}, :#{#data['city']}, :#{#data['province']}, :#{#data['country']}, :#{#data['code']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE address SET user_id = :#{#data['userId']}, street = :#{#data['street']}, district = :#{#data['district']}, city = :#{#data['city']}, province = :#{#data['province']}, country = :#{#data['country']}, code = :#{#data['code']} WHERE id = :id", nativeQuery = true)
    void putData(@Param("data") Map<String, Object> data, @Param("id") String id);

    @Transactional
    @Modifying
    @Query(value = "SELECT * FROM address WHERE user_id = :id", nativeQuery = true)
    List<Address> getDataByUserId(@Param("id") String id);
}
