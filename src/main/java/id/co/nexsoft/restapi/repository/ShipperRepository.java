package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Shipper;

public interface ShipperRepository extends JpaRepository<Shipper, Long> {
}
