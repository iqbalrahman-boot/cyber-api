package id.co.nexsoft.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.restapi.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
