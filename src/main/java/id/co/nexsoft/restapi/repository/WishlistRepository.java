package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Wishlist;

public interface WishlistRepository extends JpaRepository<Wishlist, Long> {
    @Query("SELECT u FROM Wishlist u WHERE u.userId = :id")
    List<Wishlist> findAllByRelId(@Param("id") String id);
}
