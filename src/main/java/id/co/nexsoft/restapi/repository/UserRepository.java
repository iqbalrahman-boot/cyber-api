package id.co.nexsoft.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.User;

public interface UserRepository extends JpaRepository<User, String> {
    List<User> findAllByDeletedDateIsNull();
    List<User> findAllByDeletedDateIsNotNull();
    List<User> findAllByEmail(String id);
    List<User> findAllByPhone(String phone);

    @Query("SELECT u FROM User u WHERE u.id = :id AND deletedDate IS NULL")
    Optional<User> findActiveDataById(@Param("id") String id);

    @Query("SELECT u FROM User u WHERE u.id = :id AND deletedDate IS NOT NULL")
    Optional<User> findNonActiveDataById(@Param("id") String id);
}
