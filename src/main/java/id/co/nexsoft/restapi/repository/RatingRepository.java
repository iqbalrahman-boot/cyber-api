package id.co.nexsoft.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.restapi.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    @Query("SELECT u FROM Rating u WHERE u.productId = :id")
    List<Rating> findAllByRelId(@Param("id") String id);
    
}
