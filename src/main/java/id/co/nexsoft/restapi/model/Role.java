package id.co.nexsoft.restapi.model;

public enum Role {
    ADMIN, CUSTOMER;
}