package id.co.nexsoft.restapi.auth;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.nexsoft.restapi.repository.UserRepository;
import id.co.nexsoft.restapi.jwt.GoogleRequest;
import id.co.nexsoft.restapi.jwt.JwtService;
import id.co.nexsoft.restapi.model.User;

@Service
public class AuthenticationService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;
    
    public AuthenticationResponse register(RegisterRequest request){
        List<User> userEmail = repository.findAllByEmail(request.getEmail());
        if (!userEmail.isEmpty()) {
            return null;
        }

        User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());
        user.setPassword(request.getPassword());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(request.getRole());
        user.setCreatedDate(LocalDateTime.now());
        repository.save(user);

        var jwtToken = jwtService.generateToken(user);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(jwtToken);

        return authenticationResponse;
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        User userReq = new User();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        List<User> user = repository.findAllByEmail(request.getEmail());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        userReq.setEmail(request.getEmail());
        userReq.setPassword(request.getPassword());

        if (user != null) {
            userReq.setId(user.get(0).getId());
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);
        }

        return authenticationResponse;
    }

    public AuthenticationResponse googleSignIn(GoogleRequest request) {
        User userReq = new User();
        List<User> user = repository.findAllByEmail(request.getEmail());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();

        if (!user.isEmpty()) {
            userReq.setEmail(request.getEmail());
            userReq.setPassword(user.get(0).getPassword());
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);

            return authenticationResponse;
        } else {
            User userGoogle = new User();
            userGoogle.setFirstName(request.getName());
            userGoogle.setEmail(request.getEmail());
            userGoogle.setCreatedDate(LocalDateTime.now());
            repository.save(userGoogle);

            userReq.setEmail(request.getEmail());
            userReq.setPassword(userGoogle.getPassword());
            
            var jwtToken = jwtService.generateToken(userReq);
            authenticationResponse.setToken(jwtToken);
            return authenticationResponse;
        }
    }
}