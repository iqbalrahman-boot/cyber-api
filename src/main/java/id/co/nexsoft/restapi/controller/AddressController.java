package id.co.nexsoft.restapi.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Address;
import id.co.nexsoft.restapi.service.MapStringService;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/address")
@Validated
public class AddressController extends StatusCode {
    @Autowired
    private UUIDService<Address> service;

    @Autowired
    private MapStringService<Address> customService;

    @Autowired
    private RelationStringService<Address> relService;

    @GetMapping("/all/data")
    public ResponseEntity<?> getAllData() {
        List<Address> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<Address> data = service.getAllActiveData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/nonactive")
    public ResponseEntity<?> getAllNonActiveData() {
        List<Address> data = service.getAllNonActiveData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/all/data/{id}")
    public ResponseEntity<?> getDataById(@PathVariable String id) {
        Optional<Address> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable String id) {
        Optional<Address> data = service.getActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/nonactive/{id}")
    public ResponseEntity<?> getNonActiveDataById(@PathVariable String id) {
        Optional<Address> data = service.getNonActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getDataByUserId(@PathVariable String id) {
        List<Address> data = relService.getDataByRelId(id);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable String id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Map<String, Object> data, @PathVariable String id) {
        try {
            customService.putData(data, id);
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
        }
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Map<String, Object> data) {
        Address dataPut = customService.postData(data);
        return (dataPut == null) ?
            new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY) : 
            new ResponseEntity<>(dataPut, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable String id, @RequestBody Map<String, Object> data) {
        Optional<Address> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}