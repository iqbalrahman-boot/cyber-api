package id.co.nexsoft.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.ShopAddress;
import id.co.nexsoft.restapi.model.Shop;
import id.co.nexsoft.restapi.service.LongService;
import id.co.nexsoft.restapi.service.ObjectLongService;
import id.co.nexsoft.restapi.service.CredentialService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/shop")
@Validated
public class ShopController extends StatusCode {
    @Autowired
    private LongService<Shop> service;

    @Autowired
    private LongService<ShopAddress> addressService;

    @Autowired
    private ObjectLongService<Shop> shopService;

    @Autowired
    private CredentialService<Shop> credentialService;

    @GetMapping("/all/data")
    public ResponseEntity<?> getAllData() {
        List<Shop> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<Shop> data = service.getAllActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (Shop usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/nonactive")
    public ResponseEntity<?> getAllNonActiveData() {
        List<Shop> data = service.getAllNonActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (Shop usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/all/data/{id}")
    public ResponseEntity<?> getDataById(@PathVariable Long id) {
        Optional<Shop> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable Long id) {
        Optional<Shop> data = service.getActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/nonactive/{id}")
    public ResponseEntity<?> getNonActiveDataById(@PathVariable Long id) {
        Optional<Shop> data = service.getNonActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable Long id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Shop data, @PathVariable Long id) {
        Shop dataPut = shopService.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Shop data) {
        if (
            (credentialService.getAllDataByEmail(data.getEmail()).isEmpty() && 
            credentialService.getAllDataByPhone(data.getPhone()).isEmpty()
            )) {
            if (validatePassword(data.getPassword())) {
                shopService.postData(data);
                return new ResponseEntity<>(get201(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(
                    getCustom(CODE[4], "Password doesn't valid"), 
                    HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        }
        return new ResponseEntity<>(
            getCustom(CODE[4], "username or Phone already exist"), 
            HttpStatus.UNPROCESSABLE_ENTITY
        );
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        Optional<Shop> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public static boolean validatePassword(String password) {
        String passwordRegex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).{8,12}$";
        return Pattern.matches(passwordRegex, password);
    }

    public Map<String, Object> returnData(Shop usr) {
        Map<String, Object> dataMap = new HashMap<>();
        Map<String, Object> newAddress = usr.getShopAddressId() != null ? addressData(addressService.getDataById(usr.getShopAddressId()).get()) : null;
        dataMap.put("id", usr.getId());
        dataMap.put("name", usr.getName());
        dataMap.put("description", usr.getDescription());
        dataMap.put("phone", usr.getPhone());
        dataMap.put("address", newAddress);
        return dataMap;
    }

    public Map<String, Object> addressData(ShopAddress address) {
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("street", address.getStreet());
        returnData.put("district", address.getDistrict());
        returnData.put("city", address.getCity());
        returnData.put("province", address.getProvince());
        returnData.put("country", address.getCountry());
        returnData.put("code", address.getCode());
        return returnData;
    }
}
