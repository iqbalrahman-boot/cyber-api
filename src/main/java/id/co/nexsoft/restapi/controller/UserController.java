package id.co.nexsoft.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Address;
import id.co.nexsoft.restapi.model.User;
import id.co.nexsoft.restapi.service.CredentialService;
import id.co.nexsoft.restapi.service.ObjectStringService;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/user")
@Validated
public class UserController extends StatusCode {
    @Autowired
    private UUIDService<User> service;

    @Autowired
    private UUIDService<Address> addressService;

    @Autowired
    private ObjectStringService<User> userService;

    @Autowired
    private CredentialService<User> credentialService;

    @GetMapping("/all/data")
    public ResponseEntity<?> getAllData() {
        List<User> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllActiveData() {
        List<User> data = service.getAllActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (User usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/nonactive")
    public ResponseEntity<?> getAllNonActiveData() {
        List<User> data = service.getAllNonActiveData();
        List<Map<String, Object>> dataList = new ArrayList<>();
        for (User usr : data) {
            dataList.add(returnData(usr));
        }
        return new ResponseEntity<>(dataList, HttpStatus.OK);
    }

    @GetMapping("/all/data/{id}")
    public ResponseEntity<?> getDataById(@PathVariable String id) {
        Optional<User> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable String id) {
        Optional<User> data = service.getActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @GetMapping("/nonactive/{id}")
    public ResponseEntity<?> getNonActiveDataById(@PathVariable String id) {
        Optional<User> data = service.getNonActiveDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(data.get()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable String id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody User data, @PathVariable String id) {
        User dataPut = userService.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody User data) {
        if (
            (credentialService.getAllDataByEmail(data.getEmail()).isEmpty() && 
            credentialService.getAllDataByPhone(data.getPhone()).isEmpty()
            )) {
            if (validatePassword(data.getPassword())) {
                userService.postData(data);
                return new ResponseEntity<>(get201(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(
                    getCustom(CODE[4], "Password doesn't valid"), 
                    HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        }
        return new ResponseEntity<>(
            getCustom(CODE[4], "Username or Phone already exist"), 
            HttpStatus.UNPROCESSABLE_ENTITY
        );
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable String id, @RequestBody Map<String, Object> data) {
        Optional<User> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public static boolean validatePassword(String password) {
        String passwordRegex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).{8,12}$";
        return Pattern.matches(passwordRegex, password);
    }

    public Map<String, Object> returnData(User usr) {
        Map<String, Object> dataMap = new HashMap<>();
        Map<String, Object> newAddress = usr.getAddress() != null ? addressData(addressService.getDataById(usr.getAddress()).get()) : null;
        dataMap.put("id", usr.getId());
        dataMap.put("first_name", usr.getFirstName());
        dataMap.put("last_name", usr.getLastName());
        dataMap.put("email", usr.getEmail());
        dataMap.put("phone", usr.getPhone());
        dataMap.put("role", usr.getRole());
        dataMap.put("address", newAddress);
        return dataMap;
    }

    public Map<String, Object> addressData(Address address) {
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("street", address.getStreet());
        returnData.put("district", address.getDistrict());
        returnData.put("city", address.getCity());
        returnData.put("province", address.getProvince());
        returnData.put("country", address.getCountry());
        returnData.put("code", address.getCode());
        return returnData;
    }
}
