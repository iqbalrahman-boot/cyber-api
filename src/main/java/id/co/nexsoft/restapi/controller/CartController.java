package id.co.nexsoft.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Media;
import id.co.nexsoft.restapi.model.Product;
import id.co.nexsoft.restapi.model.Shop;
import id.co.nexsoft.restapi.model.Cart;
import id.co.nexsoft.restapi.service.LongService;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.SimpleStringService;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/cart")
@Validated
public class CartController extends StatusCode {
    @Autowired
    private SimpleStringService<Cart> service;

    @Autowired
    private UUIDService<Product> productService;

    @Autowired
    private LongService<Shop> shopService;

    @Autowired
    private RelationStringService<Media> mediaRelService;

    @Autowired
    private RelationStringService<Cart> relService;

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<Map<String, Object>> returnData = new ArrayList<>();
        List<Cart> data = service.getAllData();
        for (Cart wl : data) {
            returnData.add(returnData(productService.getDataById(wl.getProductId()).get(), wl));
        }
        return new ResponseEntity<>(returnData, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable String id) {
        Optional<Cart> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(returnData(productService.getDataById(data.get().getProductId()).get(), data.get()), HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getDataByRelId(@PathVariable String id) {
        List<Map<String, Object>> returnData = new ArrayList<>();
        List<Cart> data = relService.getDataByRelId(id);
        for (Cart wl : data) {
            returnData.add(returnData(productService.getDataById(wl.getProductId()).get(), wl));
        }
        return new ResponseEntity<>(returnData, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable String id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Cart data, @PathVariable String id) {
        Cart dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Cart data) {
        Cart dataPut = service.postData(data);
        return (dataPut == null) ?
            new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY) : 
            new ResponseEntity<>(dataPut, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable String id, @RequestBody Map<String, Object> data) {
        Optional<Cart> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public Map<String, Object> returnData(Product data, Cart cart) {
        Map<String, Object> dataMap = new HashMap<>();
        Map<String, Object> newAddress = data.getShopId() != null ? shopData(shopService.getDataById(data.getShopId()).get()) : null;
        dataMap.put("id", data.getId());
        dataMap.put("shop", newAddress);
        dataMap.put("categoryId", data.getCategoryId());
        dataMap.put("name", data.getName());
        dataMap.put("description", data.getDescription());
        dataMap.put("price", data.getPrice());
        dataMap.put("discountPrice", data.getDiscountPrice());
        dataMap.put("stock", data.getStock());
        dataMap.put("media", mediaData(data.getId()));
        dataMap.put("quantity",cart.getQuantity());
        return dataMap;
    }

    public Map<String, Object> shopData(Shop data) {
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("id", data.getId());
        returnData.put("name", data.getName());
        return returnData;
    }

    public List<Map<String, Object>> mediaData(String id) {
        List<Map<String, Object>> returnData = new ArrayList<>();
        List<Media> media = mediaRelService.getDataByRelId(id);
        for (Media m : media) {
            Map<String, Object> newData = new HashMap<>();
            newData.put("id", m.getId());
            newData.put("name", m.getName());
            newData.put("url", m.getUrl());
            newData.put("type", m.getType());
            returnData.add(newData);
        }
        return returnData;
    }
}