package id.co.nexsoft.restapi.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.JDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapi.model.Rating;
import id.co.nexsoft.restapi.model.User;
import id.co.nexsoft.restapi.service.RelationStringService;
import id.co.nexsoft.restapi.service.SimpleService;
import id.co.nexsoft.restapi.service.UUIDService;
import id.co.nexsoft.restapi.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/data/rating")
@Validated
public class RatingController extends StatusCode {
    @Autowired
    private SimpleService<Rating> service;

    @Autowired
    private UUIDService<User> userService;

    @Autowired
    private RelationStringService<Rating> relService;

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<Rating> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getActiveDataById(@PathVariable Long id) {
        Optional<Rating> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data.get(), HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<?> getDataByProductId(@PathVariable String id) {
        List<Rating> data = relService.getDataByRelId(id);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable Long id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody Rating data, @PathVariable Long id) {
        Rating dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody Rating data) {
        Rating dataPut = service.postData(data);
        return (dataPut == null) ?
            new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY) : 
            new ResponseEntity<>(dataPut, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> patchData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        Optional<Rating> dataPatch = service.getDataById(id);
        try {
            if (!dataPatch.isPresent()) {
                return new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND);
            }
            service.patchData(data, id);
            return new ResponseEntity<>(get200(), HttpStatus.OK);
        } catch (JDBCException e) {
            return new ResponseEntity<>(
                getCustom(CODE[4], extractJDBCErrorMessage(e)), 
                HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public Map<String, Object> returnData(Rating data) {
        Optional<User> user = userService.getDataById(data.getUserId());
        Map<String, Object> returnData = new java.util.HashMap<>();
        returnData.put("id", data.getId());
        returnData.put("userId", user.get());
        returnData.put("rating", data.getRating());
        returnData.put("comment", data.getComment());
        return returnData;
    }
}