package id.co.nexsoft.restapi.utils;

import java.util.HashMap;
import java.util.Map;

public class StatusCode extends ErrorMessage {
    static Map<String, Object> STATUS_CODE = new HashMap<>();
    static String[] KEY = {
        "response", "message"
    };
    public static int[] CODE = {
        404, 400, 201, 200, 422
    };
    static String[] MESSAGE = {
        "Data not found", "Bad request", "Data created successfully",
        "Success", "Unprocessable Entity"
    };

    public StatusCode() {}

    public static Map<String, Object> get404() {
        return setStatus(0);
    }

    public static Map<String, Object> get201() {
        return setStatus(2);
    }

    public static Map<String, Object> get200() {
        return setStatus(3);
    }

    public static Map<String, Object> get422() {
        return setStatus(4);
    }

    public static Map<String, Object> getCustom(int code, String message) {
        STATUS_CODE.put(KEY[0], code);
        STATUS_CODE.put(KEY[1], message);
        return STATUS_CODE;
    }

    public static Map<String, Object> setStatus(int id) {
        STATUS_CODE.put(KEY[0], CODE[id]);
        STATUS_CODE.put(KEY[1], MESSAGE[id]);
        return STATUS_CODE;
    }
}
