# Rest API

User Endpoints
```
GET
[] /api/v1/data/user/all/data (all data, include non active data)
[] /api/v1/data/user (all active user)
[] /api/v1/data/user/nonactive (all nonactive user)
[] /api/v1/data/user/all/data/:id (get all user by id, include non active data)
[] /api/v1/data/user/:id (get active user by id)
[] /api/v1/data/user/nonactive/:id (get non active user by id)

DELETE, PUT, PATCH, POST
[] /api/v1/data/user/:id
```

Address endpoint
```
GET
/api/v1/data/address/all/data (all data, include non active data)
/api/v1/data/address (all active address)
/api/v1/data/address/nonactive (all nonactive address)
/api/v1/data/address/all/data/:id (get all address by id, include non active data)
/api/v1/data/address/:id (get active address by id)
/api/v1/data/address/nonactive/:id (get non active address by id)

DELETE, PUT, PATCH, POST
/api/v1/data/address/:id
```

Category endpoint
```
GET
[] /api/v1/data/category (all data)
[] /api/v1/data/category/:id (data by id)

DELETE, PUT, PATCH, POST
[] /api/v1/data/category/:id 
```

Shop Address endpoint
```
GET
[] /api/v1/data/shop/address/all/data (all data, include non active data)
[] /api/v1/data/shop/address (all active address)
[] /api/v1/data/shop/address/nonactive (all nonactive address)
[] /api/v1/data/shop/address/all/data/:id (get all address by id, include non active data)
[] /api/v1/data/shop/address/:id (get active address by id)
[] /api/v1/data/shop/address/nonactive/:id (get non active address by id)
[] /api/v1/data/shop/address/user/:id (get all address by user id)

DELETE, PUT, PATCH, POST
[] /api/v1/data/shop/address/:id
```

Shop endpoint
```
GET
[] /api/v1/data/shop/all/data (all data, include non active data)
[] /api/v1/data/shop (all active shop)
[] /api/v1/data/shop/nonactive (all nonactive shop)
[] /api/v1/data/shop/all/data/:id (get all shop by id, include non active data)
[] /api/v1/data/shop/:id (get active shop by id)
[] /api/v1/data/shop/nonactive/:id (get non active shop by id)

DELETE, PUT, PATCH, POST
[] /api/v1/data/shop/:id
```